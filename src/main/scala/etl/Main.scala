package etl

import java.nio.file.Paths

import akka.actor.ActorSystem
import akka.stream._
import akka.stream.scaladsl._
import akka.util.ByteString
import com.typesafe.scalalogging.StrictLogging

import scala.concurrent.duration._
import scala.concurrent.Future
import scala.util.Random


// This simple app demonstrates how to obtain data from some source, transform
// it, and store it elsewhere. The source is a simple counter, and the storage
// is a text file. Errors are simulated by making the transform step fail
// randomly.

object Main extends App with StrictLogging {

  logger.info(s"Process started")

  // Setup ---------------------------------------------------------------------
  implicit val system = ActorSystem("QuickStart")
  implicit val materializer = ActorMaterializer()
  implicit val ec = system.dispatcher

  // ETL steps -----------------------------------------------------------------
  val extract = Source(1 to 2500000).throttle(5000, 1.second)

  def transform(n: Int) = {
    val randomFail = Random.nextInt(10000) < 1 // ~1 in 100000 will fail
    if (randomFail) {
      Left(n)
    } else {
      Right(TransformedData("header header", n + 100))
    }
  }

  def logFailures(data: Either[Int, TransformedData]): Either[Int, TransformedData] = {
    data.left.foreach { n =>
      logger.warn(s"Failed to process entry with number $n")
    }
    data
  }

  def filterFailures(data: Either[Int, TransformedData]) = data.isRight

  def flatten(data: Either[Int, TransformedData]): TransformedData = data.right.get

  val load: Sink[TransformedData, Future[IOResult]] = Flow[TransformedData]
    .map(d => ByteString(s"${d.someHeader},s${d.transformedNumber}\n"))
    .toMat(FileIO.toPath(Paths.get("processed-data.txt")))(Keep.right)

  // Wiring and execution of the ETL -------------------------------------------
  val stream = extract
    .map(transform)
    .map(logFailures)
    .filter(filterFailures)
    .map(flatten)
    .runWith(load)

  // Shut things down when the stream completes --------------------------------
  stream.onComplete { _ =>
    logger.info(s"Process finished")
    system.terminate()
  }
}

case class TransformedData(someHeader: String, transformedNumber: Int)
