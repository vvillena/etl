# Extract, transform, load - simple proof of concept

## Regarding the challenge

There are 3 different challenges to use. All of them are extremely generic and
open-ended. I opted for the second one, named "Extract, Transform, Load!".

These are the requirements specified in the challenge:

- The ETL process will handle a minimum of 2 million data entries.
- Errors must be notified.
- Everything should be logged.
- Administrators should be able to compose graphical dashboards from the operation logs of the ETL process in time.
- The process must finish in less than 15 minutes.
- The process must not use more than 2.5 GB of memory.
- The data source must not be affected by the application pulling of data.

## Regarding the solution

Ok, the challenge is open-ended, yet at the same time, it asks for lots of
different things. I'm supposed to find some dataset, then feed it into my code.
Is this feeding part of the challenge? Should I write that dataset into some
database beforehand? Should I make my app use some API to download the data on
the fly? Which dataset do I use?

Let's keep things simple. My dataset will be the natural numbers.

Then I'm supposed to transform the data I got. Again, I'll keep things simple.
I'll attach a sample string and perform a dead simple addition on the number.

The last step is to store the transformed data somewhere. Am I supposed to feed
it to a database? Which database? Or should I send it to some sort of cloud
storage? Again, too many questions. I'll store my numbers on a text file.

With all these simplifications, the app has become a simple proof of concept.
There are some cool things left, of course. I used Akka Streams, which is very
well suited to these kinds of processes. The Akka Streams API is easy to use for
these simple use cases, and it checks all our requirements. It uses little
memory, it is fast, and it handles backpressure and throttling. The resulting
code is simple and reusable, as all the steps are coded into different functions
and variables that can be easily swapped and modified as required.

I'm not writing any tests, since this is just a proof of concept. I won't grow
this PoC into something with requirements, as that will force me to go all the
way and make the decisions I postponed before. This is a code challenge, and
growing it up to a full-fledged program will take too much time and resources.

## Requirements before running the application

This is a Scala project. You need a recent SBT version to run it.

## Running the application

Easy! Just run `sbt run`.

The app is limited to processing 5000 entries per second, which means it can
process close to 5 million entries in the 15 minutes we have available. Removing
this limitation makes the app finish its job in seconds, but I suppose it's
better to use our time and limit the load on the source.

The challenge limits our memory to 2.5 GB, you can use
sbt options to ensure the app stays under that limit, although there's no real
need, since the sbt defaults don't reserve that much memory. The app doesn't
really need that much memory (a 128 MB heap worked well on my tests, I didn't
test any further).
